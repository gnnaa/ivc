﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ivc
{   
    public interface IPoint : INotifyPropertyChanged
    {
        double X { get; set; }
        double Y { get; set; }
    }    
}
