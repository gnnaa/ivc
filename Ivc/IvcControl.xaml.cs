﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics;

namespace Ivc
{
    public partial class IvcControl : UserControl
    {
        public string MainTitle
        {
            get => lableTitle.Content as string;
            set => lableTitle.Content = value;
        }

        public double MainTitleFontSize
        {
            get => lableTitle.FontSize;
            set => lableTitle.FontSize = value;
        }

        /// <summary>
        /// Свободные точки
        /// </summary>
        public ObservableCollection<IPoint> FreePoints = new ObservableCollection<IPoint>();

        private Polyline _polylineIp = new Polyline();
        private double _ip = 1;
        public double Ip
        {
            get => _ip;
            set
            {
                _ip = value;
                UpdateIp();
            }
        }

        SolidColorBrush _brush = new SolidColorBrush(Colors.Blue);
        /// <summary>
        /// Кисть для рисования линии
        /// </summary>
        public SolidColorBrush Brush
        {
            get => _brush;
            set
            {
                _brush = value;
                UpdateGridLinesColor();
            }
        }
        double _thickness = 1;
        /// <summary>
        /// Толщина линии
        /// </summary>
        public double Thickness
        {
            get => _thickness;
            set
            {
                _thickness = value;
                UpdateGridLinesColor();
            }
        }

        public Func<double, double> ForwardConverter;
        public Func<double, double> BackConverter;

        private double _abscissaKoeff;
        private double _ordinateKoeff;
        public double FontSizeTitle { get; set; } = 12;
        public string AbscissaValuesTitleFormat { get; set; } = "{0:0.###}";
        public string OrdinateValuesTitleFormat { get; set; } = "{0:0.###}";

        public IValueConverter NaturalLogarithmConverter;

        public ObservableCollection<double?> AbscissaPoints   { get; set; } //Abscissa
        public ObservableCollection<double?> OrdinatePoints { get; set; }   //Ordinate        

        public ObservableCollection<LineViewModel> Lines { get; set; }
        private Dictionary<LineViewModel, LineData> _polylines = new Dictionary<LineViewModel, LineData>();
        private bool _isDown;
        private Point _startPoint;
        private UIElement _originalElement;
        private bool _isDragging;
        private double _originalLeft;
        private double _originalTop;
        private System.Timers.Timer _timer;
        private bool _mouseIn;

        public IvcControl()
        {
            InitializeComponent();

            ForwardConverter = x => Math.Log(x);
            BackConverter = x => Math.Exp(x);

            Lines = new ObservableCollection<LineViewModel>();
            AbscissaPoints = new ObservableCollection<double?>();
            OrdinatePoints = new ObservableCollection<double?>();

            Lines.CollectionChanged += Lines_CollectionChanged;
            AbscissaPoints.CollectionChanged += GridLinePoints_CollectionChanged;
            OrdinatePoints.CollectionChanged += GridLinePoints_CollectionChanged;

            canvas.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(canvas_PreviewMouseLeftButtonDown);
            canvas.PreviewMouseMove += new MouseEventHandler(canvas_PreviewMouseMove);
            canvas.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(canvas_PreviewMouseLeftButtonUp);
            canvas.MouseMove += Canvas_MouseMove;
            SizeChanged += IvcControl_SizeChanged;

            FreePoints.CollectionChanged += FreePoints_CollectionChanged;
        }

        private void FreePoints_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                        IPoint point = e.NewItems[i] as IPoint;
                        if (point != null)
                        {
                            if(point is UIElement uiPoint)
                            {
                                if (canvasFreePoints.Children.Contains(uiPoint) == false)
                                {
                                    point.PropertyChanged += FreePoint_PropertyChanged;
                                    canvasFreePoints.Children.Add(uiPoint);
                                    UpdateFreePoint(point);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    for (int i = 0; i < e.OldItems.Count; i++)
                    {
                        IPoint point = e.OldItems[i] as IPoint;
                        if (point != null)
                        {
                            if (point is UIElement uiPoint)
                            {
                                if (canvasFreePoints.Children.Contains(uiPoint) == true)
                                {
                                    point.PropertyChanged -= FreePoint_PropertyChanged;
                                    canvasFreePoints.Children.Remove(uiPoint);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    while (canvasFreePoints.Children.Count > 0)
                    {
                        if(canvasFreePoints.Children[0] is IPoint point)
                        {
                            point.PropertyChanged -= FreePoint_PropertyChanged;
                            canvasFreePoints.Children.RemoveAt(0);
                        }
                    }
                    break;
            }
        }

        private void FreePoint_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(sender is IPoint point)
            {
                UpdateFreePoint(point);
            }
        }

        private void UpdateFreePoint(IPoint point)
        {
            if (point is FrameworkElement ui)
            {
                double y = PhysToRel(point.Y, false) - ui.Height / 2;
                double x = PhysToRel(point.X) - ui.Width / 2;
                Canvas.SetLeft(ui, x);
                Canvas.SetTop(ui, y);
            }
        }

        private void UpdateFreePointsOnResize()
        {
            foreach (var point in FreePoints)
            {
                UpdateFreePoint(point);
            }
        }

        private void UpdateGridLinesColor()
        {
            foreach (var item in gridGrid.Children)
            {
                Polyline polyline = item as Polyline;
                if(polyline != null && polyline != _polylineIp)
                {
                    polyline.Stroke = _brush;
                    polyline.StrokeThickness = _thickness;
                }
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateMousePosition(e.GetPosition(canvas));
        }

        private double PhysToRel(double phys, bool isAbscissa = true)
        {
            if (isAbscissa)
            {
                if (phys < AbscissaPoints.First().Value)
                {
                    if(phys == 0) Debugger.Break(); // ЮР, НА ВХОД ПРИШЕЛ 0, ИДИ ВВЕРХ ПО СТЕКУ, ИЩИ ОТКУДА!!!
                    phys = AbscissaPoints.First().Value;
                }
                if(phys > AbscissaPoints.Last().Value)
                {
                    if (phys == 0) Debugger.Break(); //ЮР, НА ВХОД ПРИШЕЛ 0, ИДИ ВВЕРХ ПО СТЕКУ, ИЩИ ОТКУДА!!!
                    phys = AbscissaPoints.Last().Value;
                }
                return (ForwardConverter(phys) - ForwardConverter(AbscissaPoints.First().Value)) / _abscissaKoeff;
            }
            else
            {
                if (phys < OrdinatePoints.First().Value)
                {
                    if (phys == 0) Debugger.Break(); //НА ВХОД ПРИШЕЛ 0, ИЩИ ПО СТЕКУ ОТКУДА!!!
                    phys = OrdinatePoints.First().Value;
                }
                if (phys > OrdinatePoints.Last().Value)
                {
                    if (phys == 0) Debugger.Break(); //НА ВХОД ПРИШЕЛ 0, ИЩИ ПО СТЕКУ ОТКУДА!!!
                    phys = OrdinatePoints.Last().Value;
                }
                return gridGrid.ActualHeight - ((ForwardConverter(phys) - ForwardConverter(OrdinatePoints.First().Value)) / _ordinateKoeff);
            }
        }

        private double RelToPhys(double rel, bool isAbscissa = true)
        {
            if (isAbscissa)
                return BackConverter(rel * _abscissaKoeff + ForwardConverter(AbscissaPoints.First().Value));
            else
                return BackConverter((gridGrid.ActualHeight - rel) * _ordinateKoeff + ForwardConverter(OrdinatePoints.First().Value));
        }

        private void IvcControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateGrid();
            UpdateLines();
            UpdateIp();
        }

        private void UpdateLines()
        {
            foreach (var line in Lines)
            {
                UpdateLines(line);
            }
        }

        private void UpdateLines(LineViewModel line)
        {
            LineData lineData;            
            if (_polylines.TryGetValue(line, out lineData))
            {
                Polyline polyline = lineData.Polyline;
                for (int i = 0; i < line.Points.Count; i++)
                {
                    double y = PhysToRel(line.Points[i].Y, false);
                    double x = PhysToRel(line.Points[i].X);
                    polyline.Points[i] = new Point(x, y);
                    Canvas.SetLeft(line.Points[i], x);
                    Canvas.SetTop(line.Points[i], y);
                }
            }
        }

        private void UpdateAxis(IEnumerable<double?> points, bool isAbscissa = true)
        {
            if(points.Count() > 1)
            {
                if (isAbscissa)
                {
                    List<double> l = new List<double>();
                    foreach (var item in points)
                    {
                        double x = (ForwardConverter(item.Value) - ForwardConverter(AbscissaPoints.First().Value)) / _abscissaKoeff;
                        CreateGridPolyline(x);
                        CreateGridLineTitle(item.Value, x);
                    }
                }
                else
                {
                    List<double> l = new List<double>();
                    foreach (var item in points)
                    {
                        double y = (ForwardConverter(item.Value) - ForwardConverter(OrdinatePoints.First().Value)) / _ordinateKoeff;
                        CreateGridPolyline(y, false);
                        if (item != points.First())
                            CreateGridLineTitle(item.Value, y, false);
                        else
                            CreateGridLineTitle(item.Value, y + FontSizeTitle / 2, false);
                    }
                }
            }
        }

        private void ComputeKoeff(IEnumerable<double?> points, bool isAbscissa = true)
        {
            if (points.Count() > 1 &&
                points.First().HasValue &&
                points.Last().HasValue)
            {
                if (isAbscissa)
                {                    
                    double minX = AbscissaPoints.First().Value;
                    double maxX = AbscissaPoints.Last().Value;
                    double minXc = ForwardConverter(minX);
                    double maxXc = ForwardConverter(maxX);
                    _abscissaKoeff = (maxXc - minXc) / (gridGrid.ActualWidth - 20);
                }
                else
                {
                    double minY = OrdinatePoints.First().Value;
                    double maxY = OrdinatePoints.Last().Value;
                    double minYc = ForwardConverter(minY);
                    double maxYc = ForwardConverter(maxY);
                    _ordinateKoeff = (maxYc - minYc) / (gridGrid.ActualHeight - 20);
                }
            }
        }

        private void GridLinePoints_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateGrid();
        }

        private void UpdateIp()
        {
            if(AbscissaPoints.Count > 0)
            {
                double v = PhysToRel(_ip);
                labelIp.Margin = new Thickness(v - labelIp.Width / 2, 0, 0, 0);
                CreateIpLine(v);
            }
        }

        private void CreateIpLine(double v)
        {
            gridGrid.Children.Remove(_polylineIp);
            _polylineIp.Points.Clear();
            _polylineIp.Stroke = new SolidColorBrush(Colors.Red);
            _polylineIp.StrokeThickness = 1;

            double p = gridGrid.ActualHeight;
            _polylineIp.Points.Add(new Point(v, 0));
            _polylineIp.Points.Add(new Point(v, p));
            gridGrid.Children.Add(_polylineIp);
        }

        private void UpdateGrid()
        {
            gridGrid.Children.Clear();
            abscissaTitleGrid.Children.Clear();
            ordinateTitleGrid.Children.Clear();

            ComputeKoeff(AbscissaPoints);
            ComputeKoeff(OrdinatePoints, false);
            UpdateAxis(AbscissaPoints);
            UpdateAxis(OrdinatePoints, false);

            UpdateFreePointsOnResize();
        }

        private void CreateGridLineTitle(double? v, double offset, bool isAbscissa = true)
        {
            if (v.HasValue)
            {
                Label label = new Label();
                if (isAbscissa)
                {
                    label.Content = String.Format(AbscissaValuesTitleFormat, v.Value);
                    label.HorizontalAlignment = HorizontalAlignment.Left;
                    label.VerticalAlignment = VerticalAlignment.Top;
                    label.HorizontalContentAlignment = HorizontalAlignment.Center;
                    label.FontSize = FontSizeTitle;
                    label.MinWidth = 100;
                    Thickness tick = new Thickness();
                    tick.Left = offset - label.MinWidth / 2;
                    label.Margin = tick;
                    label.Padding = new Thickness(0);
                    abscissaTitleGrid.Children.Add(label);
                }
                else
                {
                    label.Content = String.Format(OrdinateValuesTitleFormat, v.Value);
                    label.HorizontalAlignment = HorizontalAlignment.Right;
                    label.VerticalAlignment = VerticalAlignment.Top;
                    label.FontSize = FontSizeTitle;
                    Thickness tick = new Thickness();
                    double of = offset - label.FontSize / 2 * 1.5;
                    tick.Top = gridGrid.ActualHeight - (offset + label.FontSize / 2 * 1.5);
                    tick.Right = 5;
                    label.Margin = tick;
                    label.Padding = new Thickness(0);
                    label.BorderThickness = new Thickness(0);
                    ordinateTitleGrid.Children.Add(label);
                }
            }
        }

        private void CreateGridPolyline(double? v, bool isAbscissa = true)
        {
            if (v.HasValue)
            {
                double p;
                Polyline polyline = new Polyline();
                polyline.Stroke = _brush;
                polyline.StrokeThickness = _thickness;
                if (isAbscissa)
                {
                    p = gridGrid.ActualHeight;
                    polyline.Points.Add(new Point(v.Value, 0));
                    polyline.Points.Add(new Point(v.Value, p));
                    gridGrid.Children.Add(polyline);
                }
                else
                {
                    p = gridGrid.ActualWidth;
                    polyline.Points.Add(new Point(0, gridGrid.ActualHeight - v.Value));
                    polyline.Points.Add(new Point(p, gridGrid.ActualHeight - v.Value));
                    gridGrid.Children.Add(polyline);
                }
            }
        }

        private void canvas_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (_isDown)
            {
                DragFinished(false);
                e.Handled = true;
            }
        }

        private void DragFinished(bool cancelled)
        {
            System.Windows.Input.Mouse.Capture(null);
            _isDragging = false;
            _isDown = false;
        }

        private void canvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_isDown)
            {
                if ((_isDragging == false) && ((Math.Abs(e.GetPosition(canvas).X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                    (Math.Abs(e.GetPosition(canvas).Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)))
                {
                    DragStarted();
                }
                if (_isDragging)
                {
                    DragMoved();
                }
            }
        }

        private void UpdateMousePosition(Point point)
        {
            double x = RelToPhys(point.X);
            double y = RelToPhys(point.Y, false);
            mouseCoordinates.Content = $"X:{String.Format(AbscissaValuesTitleFormat, x)}   Y:{String.Format(OrdinateValuesTitleFormat, y)}";
        }

        private void DragStarted()
        {
            _isDragging = true;
            _originalLeft = Canvas.GetLeft(_originalElement);
            _originalTop = Canvas.GetBottom(_originalElement);
        }

        private void DragMoved()
        {
            Point currentPosition = Mouse.GetPosition(canvas);
            if (currentPosition.X > 0                    &&
                currentPosition.X < canvas.ActualWidth   && 
                currentPosition.Y < canvas.ActualHeight  &&
                currentPosition.Y > 0 )
            {
                double x = RelToPhys(currentPosition.X);
                double y = RelToPhys(currentPosition.Y, false);

                Point point = new Point(x, y);
                if (_originalElement is DragPointViewModel dragPoint)
                {
                    dragPoint.PointModel.TryChangePosition(point);
                }
            }
        }

        private void canvas_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Source == canvas)
            {
            }
            else
            {
                _isDown = true;
                _startPoint = e.GetPosition(canvas);
                _originalElement = e.Source as UIElement;
                canvas.CaptureMouse();
                e.Handled = true;
            }
        }

        private LineData CreatePolyline(LineViewModel line)
        {
            Polyline polyline = new Polyline();
            polyline.Stroke = line.Brush;
            polyline.StrokeThickness = line.Thickness;
            polyline.StrokeDashArray = line.DashArray;
            polyline.StrokeLineJoin = PenLineJoin.Bevel;

            foreach (var item in line.Points)
            {
                double x = PhysToRel(item.X);
                double y = PhysToRel(item.Y, false);
                polyline.Points.Add(new Point(x, y));
            }

            lineGrid.Children.Add(polyline);
            AddPointsToCanvas(line);

            LineData ld = new LineData(polyline);
            _polylines.Add(line, ld);
            return ld;
        }

        private void AddPointsToCanvas(LineViewModel line)
        {
            foreach (var item in line.Points)
            {
                item.PointModel.PositionChenged += PointModel_PositionChenged;
                if (item.PointModel.Hiden == false)
                {
                    canvas.Children.Add(item);
                    double x = PhysToRel(item.X);
                    double y = PhysToRel(item.Y, false);
                    Canvas.SetLeft(item, x);
                    Canvas.SetTop(item, y);
                }
            }
        }

        private LineData PopLineData(LineViewModel line)
        {
            LineData lineData = null;
            if (line != null)
            {
                if(_polylines.TryGetValue(line, out lineData))
                {
                    _polylines.Remove(line);
                }
            }
            return lineData;
        }

        private void Lines_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                        LineViewModel line = e.NewItems[i] as LineViewModel;
                        if (line != null)
                        {
                            line.PropertyChanged += Line_PropertyChanged;
                            line.Points.CollectionChanged += Points_CollectionChanged;
                            LineData lineData = CreatePolyline(line);
                            if (String.IsNullOrEmpty(line.Title) == false)
                            {
                                ListViewItem listItem = CreateLineTitle(line);
                                lineData.ListItem = listItem;
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    for (int i = 0; i < e.OldItems.Count; i++)
                    {
                        LineViewModel line = e.OldItems[i] as LineViewModel;
                        if (line != null)
                        {
                            line.PropertyChanged -= Line_PropertyChanged;
                            line.Points.CollectionChanged -= Points_CollectionChanged;

                            LineData lineData = PopLineData(line);
                            if (lineData != null)
                            {                            
                                lineGrid.Children.Remove(lineData.Polyline);
                                listViewLineTitle.Items.Remove(lineData.ListItem);
                                foreach (var item in line.Points)
                                {
                                    item.PointModel.PositionChenged -= PointModel_PositionChenged;
                                    canvas.Children.Remove(item);
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    canvas.Children.Clear();
                    lineGrid.Children.Clear();
                    listViewLineTitle.Items.Clear();
                    break;
            }
        }

        private ListViewItem CreateLineTitle(LineViewModel line)
        {
            ListViewItem listItem = new ListViewItem();
            listItem.Content = line.Title;
            listItem.Foreground = line.Brush;
            listViewLineTitle.Items.Add(listItem);
            return listItem;
        }

        private void Line_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LineViewModel line = sender as LineViewModel;
            if (line != null)
            {
                LineData lineData;
                if (_polylines.TryGetValue(line, out lineData))
                {
                    if (e.PropertyName == "Brush")
                    {
                        lineData.Polyline.Stroke = line.Brush;
                        if(lineData.ListItem != null)
                            lineData.ListItem.Foreground = line.Brush;
                    }
                    else if (e.PropertyName == "Thickness")
                    {
                        lineData.Polyline.StrokeThickness = line.Thickness;
                    }
                    else if (e.PropertyName == "DashArray")
                    {
                        lineData.Polyline.StrokeDashArray = line.DashArray;
                    }
                    else if(e.PropertyName == "Title")
                    {
                        lineData.ListItem.Content = line.Title;
                    }
                }
            } 
        }

        private void PointModel_PositionChenged(object sender, EventArgs e)
        {
            PointModelBase point = sender as PointModelBase;
            if (point != null)
            {
                DragPointViewModel dragPoint;
                LineViewModel lineViewModel;
                int id = GetPointId(point, out dragPoint, out lineViewModel);
                if (id > -1)
                {
                    LineData lineData;
                    if(_polylines.TryGetValue(lineViewModel, out lineData))
                    {
                        Polyline polyline = lineData.Polyline;
                        double y = PhysToRel(dragPoint.Y, false);
                        double x = PhysToRel(dragPoint.X);
                        polyline.Points[id] = new Point(x, y);
                        Canvas.SetLeft(dragPoint, x);
                        Canvas.SetTop(dragPoint, y);
                    }
                }
            }
        }

        private void Points_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                        DragPointViewModel point = e.NewItems[i] as DragPointViewModel;
                        if (point != null)
                        {
                            LineViewModel lineViewModel = FindLine(point);
                            if(lineViewModel != null)
                            {
                                LineData lineData;
                                if (_polylines.TryGetValue(lineViewModel, out lineData))
                                {
                                    lineData.Polyline.Points.Insert(e.NewStartingIndex + i, new Point(point.X, point.Y));
                                }
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    for (int i = 0; i < e.OldItems.Count; i++)
                    {
                        DragPointViewModel point = e.OldItems[i] as DragPointViewModel;
                        if (point != null)
                        {
                            LineViewModel lineViewModel = FindLine(point);
                            if (lineViewModel != null)
                            {
                                LineData lineData;
                                if (_polylines.TryGetValue(lineViewModel, out lineData))
                                {
                                    lineData.Polyline.Points.RemoveAt(e.OldStartingIndex);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private LineViewModel FindLine(DragPointViewModel dragPoint)
        {
            LineViewModel line = null;
            foreach (var l in Lines)
            {
                foreach (var p in l.Points)
                {
                    if (dragPoint == p)
                    {
                        line = l;
                        return line;
                    }
                }
            }
            return line;
        }

        private int GetPointId(PointModelBase point, out DragPointViewModel dragPoint, out LineViewModel lineViewModel)
        {
            int id = -1;
            dragPoint = null;
            lineViewModel = null;
            foreach (var l in Lines)
            {
                for (int i=0; i< l.Points.Count; i++)
                {
                    if (point == l.Points[i].PointModel)
                    {
                        lineViewModel = l;
                        dragPoint = l.Points[i];
                        id = i;
                        return id;
                    }
                }
            }
            return id;
        }

        public MemoryStream GenerateImage()
        {
            MemoryStream file = new MemoryStream();
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)mainGrid.ActualWidth, (int)mainGrid.ActualHeight, 96d, 96d, PixelFormats.Pbgra32);
            renderBitmap.Render(mainGrid);
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
            encoder.Save(file);
            return file;
        }
    }

    public class LineData
    {
        public Polyline Polyline { get; }
        public ListViewItem ListItem { get; set; }

        public LineData(Polyline l, ListViewItem i)
        {
            Polyline = l;
            ListItem = i;
        }

        public LineData(Polyline l)
        {
            Polyline = l;
        }
    }
}
