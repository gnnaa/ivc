﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Ivc
{
    public interface IPointModel
    {
        bool Hiden { get; set; }
        double X { get; set; }
        double Y { get; set; }
        event EventHandler PositionChenged;
        void TryChangePosition(Point point);
    }

    public class PointModelBase : IPointModel
    {
        public virtual bool Hiden { get; set; }
        double _x;
        public double X
        {
            get => _x;
            set
            {
                _x = value;
                PositionChenged?.Invoke(this, EventArgs.Empty);
             }
        }
        double _y;
        public double Y
        {
            get => _y;
            set
            {
                _y = value;
                PositionChenged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler PositionChenged;
        public virtual void TryChangePosition(Point point)
        {
            X = point.X;
            Y = point.Y;
            PositionChenged?.Invoke(this, EventArgs.Empty);
        }
    }

    public class DragPointViewModel : FrameworkElement
    {
        double _radius = 3;
        public PointModelBase PointModel { get; private set; }
        public double X { get => PointModel.X; }
        public double Y { get => PointModel.Y; }

        public DragPointViewModel()
        {
        }

        public DragPointViewModel(PointModelBase point) : this()
        {
            PointModel = point;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            SolidColorBrush brush = new SolidColorBrush(Colors.Blue);
            //brush.Opacity = 0.2;
            Pen pen = new Pen(new SolidColorBrush(Colors.Navy), 1.5);
            drawingContext.DrawEllipse(brush, pen, new Point(0,0), _radius, _radius);
        }
    }

    public class LineViewModel : INotifyPropertyChanged
    {
        private string _title = null;
        /// <summary>
        /// Название кривой в легенде
        /// </summary>
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }
        SolidColorBrush _brush = new SolidColorBrush(Colors.Black);
        /// <summary>
        /// Кисть для рисования линии
        /// </summary>
        public SolidColorBrush Brush
        {
            get => _brush;
            set
            {
                _brush = value;
                OnPropertyChanged();
            }
        }
        double _thickness = 2;
        /// <summary>
        /// Толщина линии
        /// </summary>
        public double Thickness
        {
            get => _thickness;
            set
            {
                _thickness = value;
                OnPropertyChanged();
            }
        }
        DoubleCollection _dashArray = new DoubleCollection() { 1, 0 };
        /// <summary>
        /// Параметры пунктирной линии. (1, 0 - для сплошной)
        /// </summary>
        public DoubleCollection DashArray
        {
            get => _dashArray;
            set
            {
                _dashArray = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<DragPointViewModel> Points { get; set; }
        public LineViewModel()
        {
            Points = new ObservableCollection<DragPointViewModel>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
