﻿using Ivc;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeControl;

namespace IvcDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TreeBItemModel b; //для проверки изменения иконки и добавления нового эл
        TreeModel treeModel;
        public MainWindow()
        {
            InitializeComponent();
            treeModel = new TreeModel();
            TreeAItemModel a = new TreeAItemModel();
            b = new TreeBItemModel();
            a.Children.Add(b);
            treeModel.Children.Add(a);
            treeControl.ViewModel.Fill(treeModel);
            treeControl.FontSize = 12;
            //add filters
            treeControl.ViewModel.FilterCollection = new System.Collections.ObjectModel.ObservableCollection<FilterItemViewModel>();
            var f1 = new FilterItemViewModel();
            f1.Name = "By A";
            f1.Filters.Add(x => (x as TreeItemViewModel)?.Title =="A");
            var f2 = new FilterItemViewModel();
            f2.Name = "By B";
            f2.Filters.Add(x => (x as TreeItemViewModel)?.Title == "B");
            treeControl.ViewModel.FilterCollection.Add(f1);
            treeControl.ViewModel.FilterCollection.Add(f2);

            //grid
            ivcControl.AbscissaPoints.Add(0.5);
            ivcControl.AbscissaPoints.Add(1);
            ivcControl.AbscissaPoints.Add(2);
            ivcControl.AbscissaPoints.Add(5);
            ivcControl.AbscissaPoints.Add(10);
            ivcControl.AbscissaPoints.Add(50);
            ivcControl.AbscissaPoints.Add(100);

            ivcControl.OrdinatePoints.Add(0.001);
            ivcControl.OrdinatePoints.Add(0.01);
            ivcControl.OrdinatePoints.Add(0.1);
            ivcControl.OrdinatePoints.Add(1);
            ivcControl.OrdinatePoints.Add(10);
            ivcControl.OrdinatePoints.Add(100);
            ivcControl.OrdinatePoints.Add(1000);
            ivcControl.OrdinatePoints.Add(10000);
        }

        private void Button_ChangeIcon(object sender, RoutedEventArgs e)
        {
            if (b.Icon == System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\One.png"))
            {
                b.Icon = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\Two.png");
            }
            else
            {
                b.Icon = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\One.png");
            }
        }

        private void Button_Clear(object sender, RoutedEventArgs e)
        {
            treeModel.Children.Clear();
        }

        private void Button_Add(object sender, RoutedEventArgs e)
        {
            treeModel.Children.Add(new TreeAItemModel());
        }

        private void Button_Remove(object sender, RoutedEventArgs e)
        {
            if (treeModel.Children.Count > 0)
                treeModel.Children.RemoveAt(0);
        }

        private void Button_Expand(object sender, RoutedEventArgs e)
        {
            treeControl.ViewModel.SetExpandItemsCommand.Execute(true);
        }

        private void Button_Collapse(object sender, RoutedEventArgs e)
        {
            treeControl.ViewModel.SetExpandItemsCommand.Execute(false);
        }

        private void Button_ExpandPar(object sender, RoutedEventArgs e)
        {
            TreeItemViewModel treeItem = treeControl.ViewModel.ChildsViewModel.Items.FirstOrDefault(x => (x as TreeItemViewModel).Model == b) as TreeItemViewModel;
            if(treeItem != null)
            {
                treeItem.SetExpandParentsCommand.Execute(true);
            }
        }

        private void Button_AddB(object sender, RoutedEventArgs e)
        {
            b.Children.Add(new TreeAItemModel());
        }

        private void Button_PrintSelItem(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(treeControl.ViewModel.SelectedItem.ToString());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TreeItemViewModel treeItem = treeControl.ViewModel.ChildsViewModel.Items.FirstOrDefault(x => (x as TreeItemViewModel).Model == b) as TreeItemViewModel;
            if (treeItem != null)
            {
                treeControl.ViewModel.SelectedItem = treeItem;
            }
        }

        PointModelBase pm1 = new PointModelBase() { X = 1, Y = 1 };
        private void Button_AddLine(object sender, RoutedEventArgs e)
        {
            
            //DragPointViewModel p1 = new DragPointViewModel(pm1);
            DragPointViewModel p2 = new DragPointViewModel(new PointModelBase() { X = 0, Y = 100 });
            DragPointViewModel p3 = new DragPointViewModel(new PointModelBase() { X = 50, Y = 1000 });

            LineViewModel line = new LineViewModel();
            //line.Points.Add(p1);
            line.Points.Add(p2);
            line.Points.Add(p3);

            ivcControl.Lines.Add(line);
        }

        private void Button_RemoveLine(object sender, RoutedEventArgs e)
        {
            if(ivcControl.Lines.Count > 0)
            {
                ivcControl.Lines.RemoveAt(0);
            }
        }

        private void Button_LineColor(object sender, RoutedEventArgs e)
        {
            if (ivcControl.Lines.Count > 0)
            {
                ivcControl.Lines[0].Thickness = 6;
                ivcControl.Lines[0].DashArray = new DoubleCollection() { 2, 2};
                ivcControl.Lines[0].Brush = new SolidColorBrush(Colors.Blue);
                ivcControl.Thickness = 5;
                ivcControl.Brush = new SolidColorBrush(Colors.Red);
                ivcControl.MainTitle = "Красные линии!";
                ivcControl.MainTitleFontSize = 15;                
            }
        }


        
        private void Button_AddLineWithTitle(object sender, RoutedEventArgs e)
        {
            DragPointViewModel p0 = new DragPointViewModel(new PointModelBase() { X = 7, Y = 0.001, Hiden = true });
            DragPointViewModel p1 = new DragPointViewModel(new PointModelBase() { X = 7, Y = 110, Hiden = true });
            DragPointViewModel p2 = new DragPointViewModel(new PointModelBase() { X = 7.5, Y = 200, Hiden = true });
            DragPointViewModel p3 = new DragPointViewModel(new PointModelBase() { X = 8, Y = 120, Hiden = true });
            DragPointViewModel p4 = new DragPointViewModel(new PointModelBase() { X = 8, Y = 0.001, Hiden = true });

            LineViewModel line = new LineViewModel();
            line.Brush = new SolidColorBrush(Colors.DarkGreen);
            line.DashArray = new DoubleCollection() { 2, 2 };
            line.Thickness = 1;
            line.Title = "Title1";

            line.Points.Add(p0);
            line.Points.Add(p1);
            line.Points.Add(p2);
            line.Points.Add(p3);
            line.Points.Add(p4);

            ivcControl.Lines.Add(line);

            DragPointViewModel pp0 = new DragPointViewModel(new PointModelBase() { X = 9, Y = 0.001, Hiden = true });
            DragPointViewModel pp1 = new DragPointViewModel(new PointModelBase() { X = 9, Y = 110, Hiden = true });
            DragPointViewModel pp2 = new DragPointViewModel(new PointModelBase() { X = 9.5, Y = 200, Hiden = true });
            DragPointViewModel pp3 = new DragPointViewModel(new PointModelBase() { X = 11, Y = 120, Hiden = true });
            DragPointViewModel pp4 = new DragPointViewModel(new PointModelBase() { X = 11, Y = 0.001, Hiden = true });

            LineViewModel line1 = new LineViewModel();
            line1.Brush = new SolidColorBrush(Colors.DarkOrange);
            line1.DashArray = new DoubleCollection() { 4, 3 };
            line1.Thickness = 1;
            line1.Title = "Tiiiiiiiiiiiitle2";

            line1.Points.Add(pp0);
            line1.Points.Add(pp1);
            line1.Points.Add(pp2);
            line1.Points.Add(pp3);
            line1.Points.Add(pp4);

            ivcControl.Lines.Add(line1);
        }

        private void Button_ChangeIp(object sender, RoutedEventArgs e)
        {
            ivcControl.Ip = 20;
        }

        private void Button_SaveImage(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog { DefaultExt = ".png", AddExtension = true, Filter = "Png|*.png" };
            dialog.ShowDialog();

            if (dialog.FileName != string.Empty)
            {
                MemoryStream stream = ivcControl.GenerateImage();
                if(stream != null)
                {
                    using ( FileStream outStream = new FileStream(dialog.FileName, FileMode.Create) )
                    {
                        stream.WriteTo(outStream);
                        MessageBox.Show("Success!");
                    }
                }
            }
        }

        private void Button_MovePoint(object sender, RoutedEventArgs e)
        {
            pm1.X = 2;
        }

        Random xRand = new Random(35687567);
        Random yRand = new Random(111);

        private void Button_AddFreePoint(object sender, RoutedEventArgs e)
        {
            var cp = new CrossPointControl();

            //cp.X = xRand.NextDouble() * 100;
            //cp.Y = yRand.NextDouble() * 10000;
            cp.X = 50;
            cp.Y = 1;
            ivcControl.FreePoints.Add(cp);
        }

        private void Button_MoveFreePoint(object sender, RoutedEventArgs e)
        {
            foreach (var item in ivcControl.FreePoints)
            {
                item.X = xRand.NextDouble() * 100;
                item.Y = yRand.NextDouble() * 10000;
            }
        }

        private void Button_RemoveFreePoint(object sender, RoutedEventArgs e)
        {
            ivcControl.FreePoints.Clear();
        }
    }
}
