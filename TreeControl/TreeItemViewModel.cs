﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Specialized;

namespace TreeControl
{
    public enum ItemType
    {
        One,
        Two
    }

    public interface ITreeModel : INotifyPropertyChanged
    {
        string Icon { get; set; }
        string Title { get; set; }
        ContextMenu GetContextMenu();
        ObservableCollection<ITreeModel> Children { get; set; }
    }

    public class TreeItemViewModel : FilterTreeItemViewModel
    {
        public TreeItemViewModel(ITreeModel model, FilterTreeItemViewModel parent = null): base(parent)
        {
            if(model != null)
            {
                Model = model;
                Model.Children.CollectionChanged += Children_CollectionChanged;
                Model.PropertyChanged += Model_PropertyChanged;
                Icon = Model.Icon;
                Title = Model.Title;
            }
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Icon")
            {
                Icon = Model.Icon;
            }
            else if(e.PropertyName == "Title")
            {
                Title = Model.Title;
            }
        }

        protected void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int i = 0; i < e.NewItems.Count; i++)
                    {
                        Items.Add(new TreeItemViewModel(e.NewItems[i] as ITreeModel, this));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    for (int i = 0; i < e.OldItems.Count; i++)
                    {
                        var it = Items.FirstOrDefault( x => (x as TreeItemViewModel).Model == e.OldItems[i]);
                        if (it != null)
                            Items.Remove(it);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    while (Items.Count > 0)
                        Items.RemoveAt(0);
                    break;
            }
        }

        public ITreeModel Model { get; private set; }

        private string _icon;
        public string Icon
        {
            get => _icon;
            set =>  SetProperty(ref _icon, value);
        }

        private string _title;
        public string Title
        {
            get => Model.Title;
            set => SetProperty(ref _title, value);
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
