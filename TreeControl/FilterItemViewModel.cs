﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeControl
{
    public class FilterItemViewModel : BindableBase
    {
        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public List<Func<TreeItemViewModel, bool>> Filters { get; set; }

        public FilterItemViewModel()
        {
            Filters = new List<Func<TreeItemViewModel, bool>>();
        }
    }
}
