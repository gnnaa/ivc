﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TreeControl
{
    public class TreeItemModelBase : ITreeModel
    {
        protected string _icon;
        public virtual string Icon
        {
            get => _icon;
            set
            {
                _icon = value;
                OnPropertyChanged();
            }
        }

        public virtual string Title { get; set; }

        public ObservableCollection<ITreeModel> Children { get; set; } = new ObservableCollection<ITreeModel>();

        public virtual ContextMenu GetContextMenu()
        {
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = null)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    public class TreeAItemModel : TreeItemModelBase
    {
        public TreeAItemModel()
        {
            _icon = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\One.png");
        }

        public override string Title => "A";

        public override ContextMenu GetContextMenu()
        {
            return _contextMenu;
        }

        private static ContextMenu _contextMenu;

        static TreeAItemModel()
        {
            _contextMenu = new ContextMenu();
            MenuItem mi = new MenuItem();
            mi.Header = "A";
            _contextMenu.Items.Add(mi);
        }
    }

    public class TreeBItemModel : TreeItemModelBase
    {
        public TreeBItemModel()
        {
            _icon = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\Two.png");
        }

        public override string Title => "B";

        public override ContextMenu GetContextMenu()
        {
            return _contextMenu;
        }

        private static ContextMenu _contextMenu;

        static TreeBItemModel()
        {
            _contextMenu = new ContextMenu();
            MenuItem mi = new MenuItem();
            mi.Header = "B";
            _contextMenu.Items.Add(mi);
        }
    }

    public class TreeModel : TreeItemModelBase
    {
        public override string Title => "Root";
    }
}
