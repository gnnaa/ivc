﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TreeControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TreeControl : UserControl
    {
        public TreeViewModel ViewModel => DataContext as TreeViewModel;
        public TreeControl()
        {
            InitializeComponent();

            DataContext = new TreeViewModel();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox com)
            {
                FilterItemViewModel filter = com.SelectedItem as FilterItemViewModel;
                if (filter != null)
                {
                    ViewModel.ApplyFilter(filter);
                }
            }
        }

        private void TreeView_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            TreeView tv = sender as TreeView;
            if (tv != null)
            {
                Point p = e.GetPosition(tv);
                var c = tv.InputHitTest(p);
                if (c is TextBlock)
                {
                    TreeItemViewModel item = (c as TextBlock)?.DataContext as TreeItemViewModel;
                    ContextMenu cm = item?.Model.GetContextMenu();
                    if (cm != null)
                    {
                        (c as TextBlock).ContextMenu = cm;
                        (c as TextBlock).ContextMenu.IsOpen = true;
                    }                        
                    e.Handled = true;
                }
            }
        }
    }
}
