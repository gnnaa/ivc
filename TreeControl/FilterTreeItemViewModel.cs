﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TreeControl
{
    /// <summary>
    /// Коллекция с фильтрами
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FilterListViewModel<T> : BindableBase
    {
        private string _text = "Text";
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }
        private string _additionalText = "Add Text";
        public string AdditionalText
        {
            get { return _additionalText; }
            set { SetProperty(ref _additionalText, value); }  
        }
        private CollectionViewSource _source;

        /// <summary>
        /// Коллекция фильтров
        /// </summary>
        public ObservableCollection<Func<T, bool>> Filters { get; }

        /// <summary>
        /// Коллекция объектов
        /// </summary>
        public ObservableCollection<T> Items { get; }

        /// <summary>
        /// Коллекция для View
        /// </summary>
        public ICollectionView Collection { get; }

        /// <summary>
        /// Команда фильтрации
        /// </summary>
        public DelegateCommand FilterCommand { get; }

        private CancellationTokenSource _defferedFilterCTS;
        /// <summary>
        /// Отложенная фильтрация
        /// </summary>
        public DelegateCommand<TimeSpan?> DeferredFilterCommand { get; }
        public FilterListViewModel()
        {
            Items = new ObservableCollection<T>();
            Filters = new ObservableCollection<Func<T, bool>>();
            Filters.CollectionChanged += Filters_CollectionChanged;
            _source = new CollectionViewSource();
            _source.Filter += SourceFilter;
            _source.Source = Items;
            Collection = _source.View;
            FilterCommand = new DelegateCommand(FilterExecute);
            DeferredFilterCommand = new DelegateCommand<TimeSpan?>(DeferredFilterExecute);
        }
        private TimeSpan _deferredFilterTimeout = TimeSpan.FromMilliseconds(500);
        /// <summary>
        /// Таймаут между запросом на фильтрацию и запуском фильтрации
        /// </summary>
        public TimeSpan DeferredFilterTimeout
        {
            get { return _deferredFilterTimeout; }
            set { SetProperty(ref _deferredFilterTimeout, value); }
        }
        private async void DeferredFilterExecute(TimeSpan? obj)
        {
            _defferedFilterCTS?.Cancel();
            _defferedFilterCTS = new CancellationTokenSource();
            try
            {
                await Task.Delay(obj ?? DeferredFilterTimeout, _defferedFilterCTS.Token);
                FilterExecute();
            }
            catch (TaskCanceledException) { }
        }

        /// <summary>
        /// Добавление фильтра
        /// </summary>
        /// <param name="filter"></param>
        public virtual void AddFilter(Func<T, bool> filter)
        {
            Func<T, bool> f = (x) =>
            {
                if (x is T item)
                {
                    return filter(item);
                }
                return false;
            };
            Filters.Add(f);
        }
        private void Filters_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            FilterCommand?.Execute();
        }

        /// <summary>
        /// Делегат <see cref="FilterCommand"/>
        /// </summary>
        protected virtual void FilterExecute()
        {
            _defferedFilterCTS?.Cancel();
            Collection.Refresh();
        }
        private void SourceFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is T item)
            {
                e.Accepted = item == null ? false : FilterItem(item);
            }
            else
            {
                e.Accepted = false;
            }
        }

        /// <summary>
        /// Фильтр для объекта
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual bool FilterItem(T item)
        {
            foreach (var filter in Filters)
            {
                bool b = filter(item);
                if (!filter(item))
                {
                    return false;
                }
            }
            return true;
        }
    }
    public class TreeChildsViewModel : FilterListViewModel<FilterTreeItemViewModel> { }
    /// <summary>
    /// Дерево с фильтрацией
    /// </summary>
    public class FilterTreeItemViewModel : FilterListViewModel<FilterTreeItemViewModel>
    {
        public DelegateCommand DeferExpandCommand { get; }
        public DelegateCommand ApplyExpandCommand { get; }
        private bool _deferredIsExpanded;
        private bool ExpandDeferred { get; set; }
        /// <summary>
        /// Команда установки расскрытия узла
        /// </summary>
        public DelegateCommand<bool?> SetExpandCommand { get; }

        /// <summary>
        /// Команда установки расскрытия для подузлов
        /// </summary>
        public DelegateCommand<bool?> SetExpandItemsCommand { get; }

        /// <summary>
        /// Команда установки расскрытия для родительских узлов
        /// </summary>
        public DelegateCommand<bool?> SetExpandParentsCommand { get; }

        /// <summary>
        /// View модель с коллекцией всех подузлов
        /// </summary>
        public TreeChildsViewModel ChildsViewModel { get; }
        private bool _isSelected;
        /// <summary>
        /// Узел выбран
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (SetProperty(ref _isSelected, value))
                {
                    if (value)
                    {
                        Parent?.PopupSelect(this);
                    }
                    else
                    {
                        Parent?.PopupUnselect(this);
                    }
                }
            }
        }
        private bool _isExpanded;
        /// <summary>
        /// Узел расскрыт
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (!ExpandDeferred)
                {
                    SetProperty(ref _isExpanded, value);
                }
                else
                {
                    _deferredIsExpanded = value;
                }
            }
        }
        private bool _isFiltered;
        /// <summary>
        /// Узел прошел предфильтрацию
        /// </summary>
        public bool IsFiltered
        {
            get { return _isFiltered; }
            set { SetProperty(ref _isFiltered, value); }
        }
        private FilterTreeItemViewModel _parent;
        /// <summary>
        /// Родительский узел
        /// </summary>
        public FilterTreeItemViewModel Parent
        {
            get { return _parent; }
            private set { SetProperty(ref _parent, value); }
        }
        /// <summary>
        /// Корень
        /// </summary>
        public FilterTreeItemViewModel Root
        {
            get
            {
                if (Parent == null) return this;
                return Parent.Root;
            }
        }
        public FilterTreeItemViewModel(FilterTreeItemViewModel parent = null)
        {
            SetExpandCommand = new DelegateCommand<bool?>(SetExpandExecute);
            SetExpandItemsCommand = new DelegateCommand<bool?>(SetExpandItemsExecute);
            SetExpandParentsCommand = new DelegateCommand<bool?>(SetExpandParentsExecute);
            DeferExpandCommand = new DelegateCommand(DeferExpandExecute);
            ApplyExpandCommand = new DelegateCommand(ApplyExpandExecute);
            _selectedItems = new ObservableCollection<FilterTreeItemViewModel>();
            SelectedItems = new ReadOnlyObservableCollection<FilterTreeItemViewModel>(_selectedItems);
            IsFiltered = true;
            Parent = parent;
            ChildsViewModel = new TreeChildsViewModel();
            Items.CollectionChanged += Items_CollectionChanged;
            Filters.Clear();
            AddFilter(TreeFilter);
        }

        private void ApplyExpandExecute()
        {
            ExpandDeferred = false;
            foreach (var item in Items)
            {
                item.ApplyExpandExecute();
            }
            IsExpanded = _deferredIsExpanded;
        }

        private void DeferExpandExecute()
        {
            _deferredIsExpanded = IsExpanded;
            foreach (var item in Items)
            {
                item.DeferExpandExecute();
            }
            ExpandDeferred = true;
        }

        private FilterTreeItemViewModel _selectedItem;
        public FilterTreeItemViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (SetProperty(ref _selectedItem, value))
                {
                    if (value != null)
                    {
                        value.IsSelected = true;
                    }
                }
            }
        }

        private ObservableCollection<FilterTreeItemViewModel> _selectedItems;
        public ReadOnlyObservableCollection<FilterTreeItemViewModel> SelectedItems { get; }
        public event EventHandler<FilterTreeItemViewModel> ItemSelected;
        public event EventHandler<FilterTreeItemViewModel> ItemUnselected;
        private void PopupSelect(FilterTreeItemViewModel item)
        {
            SelectedItem = item;
            _selectedItems.Add(item);
            ItemSelected?.Invoke(this, item);
            Parent?.PopupSelect(item);
        }

        private void PopupUnselect(FilterTreeItemViewModel item)
        {
            Parent?.PopupUnselect(item);
            _selectedItems.Remove(item);
            if (SelectedItem == item)
            {
                SelectedItem = SelectedItems.FirstOrDefault();
            }
            ItemUnselected?.Invoke(this, item);
        }
        protected void SetExpandParentsExecute(bool? obj)
        {
            if (obj.HasValue && Parent != null)
            {
                Parent.SetExpandExecute(obj);
                Parent.SetExpandParentsExecute(obj);
            }
        }

        protected void SetExpandItemsExecute(bool? obj)
        {
            SetExpandExecute(obj);
            if (obj.HasValue)
            {
                foreach (var item in Items)
                {
                    item.SetExpandItemsExecute(obj);
                }
            }
        }

        protected void SetExpandExecute(bool? obj)
        {
            if (obj.HasValue)
            {
                IsExpanded = obj.Value;
            }
        }

        /// <summary>
        /// Фильтр дерева (оставляем все отфильтрованные или не пустые узлы)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual bool TreeFilter(FilterTreeItemViewModel item)
        {
            return item.IsFiltered || !item.Collection.IsEmpty;
        }

        /// <summary>
        /// Событие добавления подузла
        /// </summary>
        public event EventHandler<FilterTreeItemViewModel> ChildAdded;

        /// <summary>
        /// Событие удаления подузла
        /// </summary>
        public event EventHandler<FilterTreeItemViewModel> ChildRemoved;

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                    {
                        if (item is FilterTreeItemViewModel x)
                        {
                            x.Parent = this;
                            ChildsViewModel.Items.Add(x);
                            foreach (var child in x.ChildsViewModel.Items)
                            {
                                ChildsViewModel.Items.Add(child);
                            }
                            x.ChildAdded += X_ItemAdded;
                            x.ChildRemoved += X_ItemRemoved;
                            ChildAdded?.Invoke(this, x);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        if (item is FilterTreeItemViewModel x)
                        {
                            x.IsSelected = false;
                            x.Parent = null;
                            x.ChildAdded -= X_ItemAdded;
                            x.ChildRemoved -= X_ItemRemoved;
                            foreach (var child in x.ChildsViewModel.Items)
                            {
                                ChildsViewModel.Items.Remove(child);
                            }
                            ChildsViewModel.Items.Remove(x);
                            ChildRemoved?.Invoke(this, x);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    while (ChildsViewModel.Items.Count > 0)
                    {
                        ChildsViewModel.Items.First().ChildAdded -= X_ItemAdded;
                        ChildsViewModel.Items.First().ChildRemoved -= X_ItemRemoved;
                        ChildsViewModel.Items.RemoveAt(0);
                    }
                    break;
            }
        }

        private void X_ItemRemoved(object sender, FilterTreeItemViewModel e)
        {
            ChildsViewModel.Items.Remove(e);
            ChildRemoved?.Invoke(this, e);
        }

        private void X_ItemAdded(object sender, FilterTreeItemViewModel e)
        {
            ChildsViewModel.Items.Add(e);
            ChildAdded?.Invoke(this, e);
        }

        public FilterTreeItemViewModel() : this(null) { }

        private bool _isExpandedBeforeFilter;
        public bool IsExpandedBeforeFilter
        {
            get { return _isExpandedBeforeFilter; }
            set { SetProperty(ref _isExpandedBeforeFilter, value); }
        }

        /// <summary>
        /// Переопределение фильтрации
        /// </summary>
        protected override void FilterExecute()
        {
            IsExpandedBeforeFilter = IsExpanded;
            TreeFilterExecute();
            IsExpanded = IsExpandedBeforeFilter;
        }

        /// <summary>
        /// Затем фильтруется коллекция подузлов
        /// </summary>
        protected virtual void TreeFilterExecute()
        {
            TreePreFilterExecute();
            foreach (var item in Items)
            {
                item.FilterExecute();
            }
            base.FilterExecute();
        }

        /// <summary>
        /// Все подузлы проходят префильтрацию (для корневого узла)
        /// </summary>
        protected virtual void TreePreFilterExecute()
        {
            if (Parent == null)
            {
                foreach (var item in ChildsViewModel.Items)
                {
                    item.IsFiltered = PreTreeItemFilter(item);
                }
            }
        }

        /// <summary>
        /// Префильтрация узла
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected virtual bool PreTreeItemFilter(FilterTreeItemViewModel item) { return true; }
    }
}
