﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeControl
{
    public class TreeViewModel : TreeItemViewModel
    {
        public TreeViewModel() : base(null)
        {
            FilterCollection = new ObservableCollection<FilterItemViewModel>();
        }

        public void GenerateTestData()
        { 
        }

        /// <summary>
        /// Заполняет дерево по моделе. На вход корневой элемент
        /// </summary>
        /// <param name="root"></param>
        public void Fill(ITreeModel root)
        {
            Items.Clear();
            ChildsViewModel.Items.Clear();
            TreeItemViewModel ni = new TreeItemViewModel(root, this);
            Items.Add(ni);
            foreach (var item in root.Children)
                FillRecursive(item, ni);
        }

        public void FillRecursive(ITreeModel root, TreeItemViewModel iRoot)
        {
            TreeItemViewModel ni = new TreeItemViewModel(root, iRoot);
            iRoot.Items.Add(ni);
            foreach (var item in root.Children)
                FillRecursive(item, ni);
        }

        public ObservableCollection<FilterItemViewModel> FilterCollection { get; set; }

        private FilterItemViewModel _selectedFilter;
        public FilterItemViewModel SelectedFilter { get; set; }

        public void ApplyFilter(FilterItemViewModel filter)
        {
            SelectedFilter = filter;
            FilterExecute();
        }

        protected override bool PreTreeItemFilter(FilterTreeItemViewModel item)
        {
            bool ret = true;
            if(SelectedFilter != null)
            {
                foreach (var filter in SelectedFilter.Filters)
                {
                    if((item as TreeItemViewModel) != null)
                    {
                        bool b = filter(item as TreeItemViewModel);
                        if (b == false)
                        {
                            ret = false;
                        }
                    }
                }
            }
            return ret;
        }
    }
}
